<?php

/**
 * @file
 * Administration form for password history module.
 */

/**
 * Page callback for password history form.
 */
function password_history_settings_form($form, &$form_state) {
  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Password History Settings'),
    '#description' => t('Optionally display email history including date and subject.'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['settings']['password_history_display_email_history'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display email history regarding password information.'),
    '#description' => t('This requires the <a href="@url" target="_blank">mail logger</a>  module.',
      array('@url' => 'https://www.drupal.org/project/mail_logger')
    ),
    '#default_value' => variable_get('password_history_display_email_history', 0),
  );

  return system_settings_form($form);
}

/**
 * Validations for the password history settings form.
 */
function password_history_settings_form_validate($form, &$form_state) {
  // Validate the mail logger module is installed if display email is selected.
  if ($form_state['values']['password_history_display_email_history'] === 1 && !module_exists('mail_logger')) {
    $message = t('Email history requires the <a href="@url" target="_blank">mail logger</a>  module to be installed and enabled.',
      array('@url' => 'https://www.drupal.org/project/mail_logger'));
    form_set_error('password_history_display_email_history', $message);
  }
}
