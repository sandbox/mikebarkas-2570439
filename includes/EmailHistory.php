<?php

/**
 * @file
 * Contains the PasswordHistoryEmailHistory class.
 */

/**
 * Establishes the PasswordHistoryEmailHistory class.
 *
 * Provides user email history regarding password data.
 */
class PasswordHistoryEmailHistory {

  protected $user;

  /**
   * Set user object with an entity wrapper.
   *
   * @param object $user_wrapper
   *   The Drupal user object entity wrapper.
   */
  public function __construct($user_wrapper) {
    $this->user = $user_wrapper;
  }

  /**
   * Determines email exists.
   */
  public function emailAddressExists() {
    if ($this->user->mail->value() == NULL) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Renders table of user's email history.
   */
  public function renderEmailHistoryTable() {
    // Don't display table if email address doesn't exist.
    if ($this->user->mail->value() != NULL) {
      $header[] = array('data' => t('Date'));
      $header[] = array('data' => t('Email Subject'));
      // Generate table rows.
      if ($this->emailHistory()) {
        $i = 0;
        foreach ($this->emailHistory() as $history) {
          $row[$i]['date_sent'] = format_date($history['date_sent'], 'long');
          $row[$i]['subject'] = $history['subject'];
          $i++;
        }
      }
      // If no email records, display a message in table.
      if (!isset($row)) {
        $colspan = '2';
        $row[] = array(array('data' => t('No email history to display'), 'colspan' => $colspan));
      }
      return theme('table', array('header' => $header, 'rows' => $row));
    }
    return t('No email address exists for this user.');
  }

  /**
   * Get user's email history.
   *
   * Returns email history regarding password resets or false.
   */
  private function emailHistory() {
    // Only messages for password policy warnings and password resets.
    $message_types = array('user_password_reset', 'password_policy_warning');

    $query = db_select('mail_logger', 'm')
      ->fields('m', array('subject', 'date_sent'))
      ->condition('mailkey', $message_types, 'IN')
      ->condition('mailto', $this->user->mail->value());
    $email_history = $query->execute()->fetchAll();
    $email_data = array();
    if (isset($email_history) && !empty($email_history)) {
      foreach ($email_history as $email) {
        $email_data[] = array(
          'subject' => $email->subject,
          'date_sent' => $email->date_sent,
        );
      }
      return $email_data;
    }
    return FALSE;
  }

}
