<?php

/**
 * @file
 * Contains the PasswordHistory class.
 */

/**
 * Establishes the PasswordHistory class.
 *
 * Provides basic user password history data.
 */
class PasswordHistory {

  protected $user;

  /**
   * Set user object with an entity wrapper.
   *
   * @param object $user_wrapper
   *   The Drupal user object entity wrapper.
   */
  public function __construct($user_wrapper) {
    $this->user = $user_wrapper;
  }

  /**
   * Returns Drupal status.
   */
  public function getDrupalStatus() {
    return $this->user->status->value() ? 'Active' : 'Blocked';
  }

  /**
   * Formats date or provide a default value.
   *
   * @param string $timestamp
   *   Unix timestamp.
   */
  public function formatDate($timestamp) {
    if ($timestamp > 0) {
      return format_date($timestamp, 'long');
    }
    return t('No date available');
  }

  /**
   * Renders password history list.
   */
  public function renderPasswordHistoryList() {
    $items = array();
    // If no password history records, return a message.
    if ($this->passwordHistory()) {
      foreach ($this->passwordHistory() as $timestamp) {
        $items[] = format_date($timestamp, 'long');
      }
      return $items;
    }
    return $items[] = 'No password history to display.';
  }

  /**
   * Get user's password history.
   *
   * Returns password policy date history or false.
   */
  private function passwordHistory() {
    $query = db_select('password_policy_history', 'p')
      ->fields('p', array('created'))
      ->condition('uid', $this->user->uid->value());
    $password_history = $query->execute()->fetchCol();
    $history = array();
    // Return history or false.
    if (isset($password_history)) {
      foreach ($password_history as $timestamp) {
        $history[] = $timestamp;
      }
      return $history;
    }
    return FALSE;
  }

}
