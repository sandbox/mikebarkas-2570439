<?php

/**
 * @file
 * Unit tests for password history module.
 */

/**
 * Tests for PasswordHistoryEmailHistory class.
 */
class PasswordHistoryEmailHistoryTest extends PHPUnit_Framework_TestCase {

  /**
   * Meta information for tests.
   */
  public static function getInfo() {
    return array(
      'name' => 'Password History module',
      'description' => 'Test the Password History module.',
      'group' => 'Password History',
    );
  }
  /**
   * Test emailAddressExists().
   *
   * @group passwordhistory
   */
  public function testEmailAddressExists() {
    $stub = $this->getMockBuilder('PasswordHistoryEmailHistory')
      ->getMock();

    $stub->method('emailAddressExists')
      ->willReturn('TRUE');

    $this->assertEquals('TRUE', $stub->emailAddressExists());
  }

}
