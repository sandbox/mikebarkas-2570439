<?php

/**
 * @file
 * Unit tests for password history module.
 */

/**
 * Tests for PasswordHitory class.
 */
class PasswordHistoryTest extends PHPUnit_Framework_TestCase {


  /**
   * Meta information for tests.
   */
  public static function getInfo() {
    return array(
      'name' => 'Password History Module',
      'description' => 'Test the Password History module.',
      'group' => 'Password History',
    );
  }

  /**
   * Test getDrupalStatus().
   *
   * @group passwordhistory
   */
  public function testGetDrupalStatus() {
    $stub = $this->getMockBuilder('PasswordHistory')
      ->getMock();
    $stub->method('getDrupalStatus')
      ->will($this->onConsecutiveCalls('Active', 'Blocked'));

    $this->assertEquals('Active', $stub->getDrupalStatus());
    $this->assertEquals('Blocked', $stub->getDrupalStatus());
  }

  /**
   * Test formatDate().
   *
   * @group passwordhistory
   */
  public function testFormatDate() {
    $stub = $this->getMockBuilder('PasswordHistory')
      ->setMethods(NULL)
      ->getMock();

    $epoch_timestamp = '1294228800';
    $gmt_time = 'Wednesday, January 5, 2011 - 7:00am';
    $no_date = 'No date available';

    $this->assertEquals($gmt_time, $stub->formatDate('1294228800'), 'Epoch timestamp parameter');
    $this->assertEquals($no_date, $stub->formatDate('bad-string'), 'Incorrect string parameter');
    $this->assertEquals($no_date, $stub->formatDate('0'), 'String 0 parameter');
    $this->assertEquals($no_date, $stub->formatDate(0), 'Integer 0 parameter');
  }

}
